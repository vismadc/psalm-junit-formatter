<?php

/**
 * @copyright Visma Digital Commerce AS 2020
 * @license   Proprietary
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\PsalmJUnitFormatter;

use SebastianBergmann\Comparator\DOMNodeComparator;
use Visma\PsalmJUnitFormatter\Exception\InvalidNodeValueException;
use Visma\PsalmJUnitFormatter\Exception\NoSuchChildNodeException;

trait XMLParsingTrait
{
    /**
     * Performs case-insensitive matching over the child node names and returns
     * the first match
     *
     * @param \DOMNode $node
     * @param string   $name
     * @return \DOMNode
     * @throws Exception\NoSuchChildNodeException
     */
    protected function getFirstNamedXMLChildNode(\DOMNode $node, string $name): \DOMNode
    {
        /** @var \DOMNode $node */
        foreach ($node->childNodes as $node) {
            if (strcasecmp($node->nodeName, $name) === 0) {
                return $node;
            }
        }

        throw NoSuchChildNodeException::named($name);
    }

    /**
     * Helper method for fetching a string value from an XML child n ode
     *
     * @param \DOMNode $item
     * @param string   $name
     * @throws \Visma\PsalmJUnitFormatter\Exception\NoSuchChildNodeException
     * @throws \Visma\PsalmJUnitFormatter\Exception\InvalidNodeValueException
     */
    protected function getXMLChildNodeStringValue(\DOMNode $item, string $name): string
    {
        $node = $this->getFirstNamedXMLChildNode($item, $name);
        $value = $node->nodeValue;

        if ((strlen($value) > 0)) {
            return $value;
        }

        throw new InvalidNodeValueException();
    }


    /**
     * Helper method for fetching an integer value from an XML child n ode
     *
     * @param \DOMNode $item
     * @param string   $name
     * @throws \Visma\PsalmJUnitFormatter\Exception\NoSuchChildNodeException
     * @throws \Visma\PsalmJUnitFormatter\Exception\InvalidNodeValueException
     */
    protected function getXMLChildNodeIntValue(\DOMNode $item, string $name): int
    {
        $node = $this->getFirstNamedXMLChildNode($item, $name);
        $value = $node->nodeValue;

        if (is_numeric($value)) {
            return (int)$value;
        }

        throw new InvalidNodeValueException();
    }

    public function getXMLChildNodeStringValueOrNull(\DOMNode $item, string $name): ?string
    {
        try {
            return $this->getXMLChildNodeStringValue($item, $name);
        } catch (InvalidNodeValueException | NoSuchChildNodeException $e) {
            return null;
        }
    }
}
