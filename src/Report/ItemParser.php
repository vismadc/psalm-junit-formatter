<?php

/**
 * @copyright Visma Digital Commerce AS 2020
 * @license   Proprietary
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\PsalmJUnitFormatter\Report;

use Visma\PsalmJUnitFormatter\XMLParsingTrait;

class ItemParser
{
    use XMLParsingTrait;

    public function parseDOMNode(\DOMNode $item): Item
    {
        return new Item(
            $this->getXMLChildNodeStringValue($item, "severity"),
            $this->getXMLChildNodeStringValue($item, "type"),
            $this->getXMLChildNodeStringValue($item, "message"),
            $this->getXMLChildNodeStringValue($item, "file_name"),
            $this->getXMLChildNodeStringValue($item, "file_path"),
            $this->getXMLChildNodeIntValue($item, "error_level"),
            $this->getXMLChildNodeStringValueOrNull($item, "link")
        );
    }
}
