<?php

/**
 * @copyright Visma Digital Commerce AS 2020
 * @license   Proprietary
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\PsalmJUnitFormatter\Report;

/**
 * A single issue in a Psalm report
 */
class Item
{
    /** @var string */
    private $severity;

    /** @var string */
    private $type;

    /** @var string */
    private $message;

    /** @var string */
    private $fileName;

    /** @var string */
    private $filePath;

    /** @var int */
    private $errorLevel;

    /** @var string|null */
    private $link;

    /**
     * @param string      $severity
     * @param string      $type
     * @param string      $message
     * @param string      $fileName
     * @param string      $filePath
     * @param int         $errorLevel
     * @param string|null $link
     */
    public function __construct(
        string $severity,
        string $type,
        string $message,
        string $fileName,
        string $filePath,
        int $errorLevel = -1,
        ?string $link = null
    ) {
        $this->severity = $severity;
        $this->type = $type;
        $this->message = $message;
        $this->fileName = $fileName;
        $this->filePath = $filePath;
        $this->errorLevel = $errorLevel;
        $this->link = $link;
    }

    /**
     * Create a new instance with the provided severity
     *
     * @param string $severity
     * @return \Visma\PsalmJUnitFormatter\Report\Item
     */
    public function withSeverity(string $severity): Item
    {
        $item = clone $this;
        $item->severity = $severity;
        return $item;
    }

    /**
     * Create a new instance with the provided type
     *
     * @param string $type
     * @return \Visma\PsalmJUnitFormatter\Report\Item
     */
    public function withType(string $type): Item
    {
        $item = clone $this;
        $item->type = $type;
        return $item;
    }

    /**
     * Create a new instance with the provided message
     *
     * @param string $message
     * @return \Visma\PsalmJUnitFormatter\Report\Item
     */
    public function withMessage(string $message): Item
    {
        $item = clone $this;
        $item->message = $message;
        return $item;
    }

    /**
     * Create a new instance with the provided fileName
     *
     * @param string $fileName
     * @return \Visma\PsalmJUnitFormatter\Report\Item
     */
    public function withFileName(string $fileName): Item
    {
        $item = clone $this;
        $item->fileName = $fileName;
        return $item;
    }

    /**
     * Create a new instance with the provided filePath
     *
     * @param string $filePath
     * @return \Visma\PsalmJUnitFormatter\Report\Item
     */
    public function withFilePath(string $filePath): Item
    {
        $item = clone $this;
        $item->filePath = $filePath;
        return $item;
    }

    /**
     * Create a new instance with the provided errorLevel
     *
     * @param int $errorLevel A signed integer
     * @return \Visma\PsalmJUnitFormatter\Report\Item
     */
    public function withErrorLevel(int $errorLevel): Item
    {
        $item = clone $this;
        $item->errorLevel = $errorLevel;
        return $item;
    }

    /**
     * Create a new instance with the provided link
     *
     * @param string|null $link
     * @return \Visma\PsalmJUnitFormatter\Report\Item
     */
    public function withLink(?string $link): Item
    {
        $item = clone $this;
        $item->link = $link;
        return $item;
    }

    /**
     * @return string
     */
    public function getSeverity(): string
    {
        return $this->severity;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }

    /**
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }

    /**
     * @return int
     */
    public function getErrorLevel(): int
    {
        return $this->errorLevel;
    }

    /**
     * @return string|null
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @param \DOMDocument $dom
     * @return \DOMNode
     */
    public function toTestCase(\DOMDocument $dom): \DOMNode
    {
        $case = $dom->createElement("testcase");
        $case->setAttribute("name", sprintf("%s at %s", $this->getType(), $this->getFileName()));
        $case->appendChild($this->getFailure($dom));

        return $case;
    }

    /**
     * Get the "failure" XML subnode
     *
     * @param \DOMDocument $dom
     * @return \DOMNode
     */
    private function getFailure(\DOMDocument $dom): \DOMNode
    {
        $failure = $dom->createElement("failure");
        $failure->setAttribute("type", $this->getSeverity());
        $message = $this->getMessage();
        if (($link = $this->getLink()) !== null) {
            $message = sprintf("%s (%s)", $message, $link);
        }
        $failure->nodeValue = $message;

        return $failure;
    }
}
