<?php

/**
 * @copyright Visma Digital Commerce AS 2020
 * @license   Proprietary
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\PsalmJUnitFormatter\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\ConsoleOutputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Visma\PsalmJUnitFormatter\Exception\FileNotReadableException;
use Visma\PsalmJUnitFormatter\Exception\NoSuchFileException;
use Visma\PsalmJUnitFormatter\ReportParser;

/**
 * Class Format
 */
class Format extends Command
{
    public const ARGUMENT_FILENAME = "filename";

    /**
     * @var \Visma\PsalmJUnitFormatter\ReportParser
     */
    protected $reportParser;

    /**
     * @var string
     */
    private $helpText = <<<HELPTEXT
Reads the provided XML report from Psalm, either supplied as an argument to the
application or from stdin, and writes a JUnit-readable XML report to stdout or
the desired filename.  

HELPTEXT;

    public function __construct(
        ReportParser $reportParser,
        string $name = null
    ) {
        parent::__construct($name);
        $this->reportParser = $reportParser;
    }

    protected function configure(): void
    {
        parent::configure();
        $this->addArgument(self::ARGUMENT_FILENAME, InputArgument::OPTIONAL, "Read from this file");
        $this->addOption("output", "o", InputOption::VALUE_REQUIRED, "Write output to this file");
        $this->setHelp($this->helpText);
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface   $input
     * @param \Symfony\Component\Console\Output\OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        try {
            $filename = $this->getFilename($input);
            $this->validateFilename($filename);
            $report = $this->reportParser->parseXMLFile($filename);
        } catch (\InvalidArgumentException $e) {
            // Read from stdin
            $stdin = fopen("php://stdin", "r");
            if ($stdin === false) {
                throw new \Exception("Could not read from stdin");
            }
            $contents = stream_get_contents($stdin);
            fclose($stdin);
            $report = $this->reportParser->parseXMLString($contents);
        }

        $domDoc = $report->toDOMDocument();

        $xml = $domDoc->saveXML();

        if ($xml === false) {
            throw new \Exception("Could not format report as XML");
        }

        $out = $this->getOutput($input, $output);

        if (is_resource($out)) {
            fwrite($out, $xml);
            fclose($out);
        } else {
            $out->write($xml);
        }

        return 0;
    }

    /**
     * @param \Symfony\Component\Console\Input\InputInterface $input
     */
    private function getFilename(InputInterface $input): string
    {
        $filename = $input->getArgument(self::ARGUMENT_FILENAME);
        if (is_string($filename)) {
            return $filename;
        }

        throw new \InvalidArgumentException("No filename provided");
    }

    /**
     * @return OutputInterface|resource
     */
    protected function getOutput(InputInterface $input, OutputInterface $output)
    {
        $outputFilename = $input->getOption("output");
        if (is_string($outputFilename)) {
            $handle = fopen($outputFilename, "w");
            if (!$handle) {
                throw new \InvalidArgumentException("Could not open {$outputFilename} for writing");
            }
            return $handle;
        }

        return $output;
    }

    /**
     * @param string $filename
     * @return void
     */
    private function validateFilename(string $filename): void
    {
        if (!file_exists($filename)) {
            throw new NoSuchFileException("Could not find file {$filename}");
        }

        if (!is_readable($filename)) {
            throw new FileNotReadableException("Could not read file {$filename}");
        }
    }
}
