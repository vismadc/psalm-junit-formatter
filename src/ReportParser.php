<?php

/**
 * @copyright Visma Digital Commerce AS 2020
 * @license   Proprietary
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\PsalmJUnitFormatter;

use Visma\PsalmJUnitFormatter\Report\ItemParser;

class ReportParser
{
    use XMLParsingTrait;

    /**
     * @var \Visma\PsalmJUnitFormatter\Report\ItemParser
     */
    protected $itemParser;
    /**
     * @var string
     */
    protected $schemaFile;

    public function __construct(ItemParser $itemParser, string $schemaFile)
    {
        $this->itemParser = $itemParser;
        $this->schemaFile = $schemaFile;
    }

    /**
     * @param string $filename
     * @return Report
     */
    public function parseXMLFile(string $filename): Report
    {
        return $this->parseXMLString(file_get_contents($filename));
    }

    public function parseXMLString(string $contents): Report
    {
        $doc = new \DOMDocument();
        $doc->loadXML($contents);

        $items = [];

        foreach ($this->getItems($doc) as $item) {
            $items[] = $item;
        }

        return new Report($this->schemaFile, $items);
    }

    /**
     * @param \DOMDocument $doc
     * @return \Generator
     * @psalm-return \Generator<\Visma\PsalmJUnitFormatter\Report\Item>
     * @throws \Visma\PsalmJUnitFormatter\Exception\NoSuchChildNodeException
     */
    private function getItems(\DOMDocument $doc): \Generator
    {
        $report = $this->getFirstNamedXMLChildNode($doc, "report");
        /** @var \DOMNode $item */
        foreach ($report->childNodes as $item) {
            if ($item->nodeName === "item") {
                yield $this->itemParser->parseDOMNode($item);
            }
        }
    }
}
