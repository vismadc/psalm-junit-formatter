<?php

/**
 * @copyright Visma Digital Commerce AS 2020
 * @license   Proprietary
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\PsalmJUnitFormatter;

use Visma\PsalmJUnitFormatter\Report\Item;

class Report
{
    /**
     * @var string
     */
    protected $schemaFile;
    /**
     * @var Item[]
     * @psalm-var list<Item>
     */
    private $items;

    /**
     * Report constructor.
     *
     * @param string $schemaFile
     * @param Item[] $items
     */
    public function __construct(string $schemaFile, array $items = [])
    {
        $this->items = array_values($items);
        $this->schemaFile = $schemaFile;
    }

    /**
     * @return \DOMDocument
     */
    public function toDOMDocument(): \DOMDocument
    {
        $document = new \DOMDocument();
        $suite = $this->getTestsuite($document);
        $document->appendChild($suite);

        $document->schemaValidate($this->schemaFile);

        return $document;
    }

    /**
     *
     */
    private function getTestsuite(\DOMDocument $dom): \DOMNode
    {
        $suite = $dom->createElement("testsuite");

        $tests = 0;
        $failures = 0;
        $errors = 0;
        $skipped = 0;
        foreach ($this->items as $item) {
            $tests++;
            if ($item->getSeverity() === "error") {
                $case = $item->toTestCase($dom);
                $failures++;
                $suite->appendChild($case);
            }
        }

        $suite->setAttribute("name", "psalm");
        $suite->setAttribute("tests", (string)$tests);
        $suite->setAttribute("failures", (string)$failures);

        return $suite;
    }
}
