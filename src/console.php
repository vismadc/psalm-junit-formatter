<?php

/**
 * @copyright Visma Digital Commerce AS 2020
 * @license   Proprietary
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

$autoloader = implode(DIRECTORY_SEPARATOR, [dirname(__FILE__, 2), "vendor", "autoload.php"]);
if (!file_exists($autoloader)) {
    throw new \Exception("Could not locate autoloader");
}

require_once $autoloader;

// Set up dependencies
$schemaFile = implode(DIRECTORY_SEPARATOR, [dirname(__FILE__, 2), "resources", "junit.xsd"]);
$itemParser = new \Visma\PsalmJUnitFormatter\Report\ItemParser();
$reportParser = new \Visma\PsalmJUnitFormatter\ReportParser($itemParser, $schemaFile);

// Main formatting command
$format = new \Visma\PsalmJUnitFormatter\Command\Format($reportParser, "format");

// Set up and run application
$application = new \Symfony\Component\Console\Application("psalm-junit-formatter");
$application->add($format);
$application->setDefaultCommand("format", true);

$application->run();
