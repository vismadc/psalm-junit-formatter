<?php

/**
 * @copyright Visma Digital Commerce AS 2020
 * @license   Proprietary
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\PsalmJUnitFormatter\Exception;

class NoSuchChildNodeException extends NoSuchEntityException
{

    public static function named(string $name): NoSuchChildNodeException
    {
        return new static("No child node named {$name}");
    }
}
