# visma/psalm-junit-formatter

Simple tool to format a psalm XML report using the JUnit format, allowing the 
test reports to be reported in Bitbucket Pipelines and Jenkins.

## Usage

`php psalm-junit-formatter.phar --help`
