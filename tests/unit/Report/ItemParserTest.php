<?php

/**
 * @copyright Visma Digital Commerce AS 2020
 * @license   Proprietary
 * @author    Marcus Pettersen Irgens <marcus.irgens@visma.com>
 */

declare(strict_types=1);

namespace Visma\PsalmJUnitFormatter\Report;

use PHPUnit\Framework\TestCase;
use Visma\PsalmJUnitFormatter\XMLParsingTrait;

/**
 * @covers \Visma\PsalmJUnitFormatter\Report\ItemParser
 */
class ItemParserTest extends TestCase
{
    use XMLParsingTrait;

    public function testCanParseItem()
    {
        $dom = new \DOMDocument();
        $dom->load(__DIR__ . '/Fixtures/valid_item.xml');
        $itemData = $this->getFirstNamedXMLChildNode($dom, "item");

        $parser = new ItemParser();

        $item = $parser->parseDOMNode($itemData);

        $this->assertEquals("error", $item->getSeverity());
    }
}
